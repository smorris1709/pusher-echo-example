<?php
use App\Events\UserUpdated;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/fire', function () {

    $user = User::findOrFail(1);
    event(new UserUpdated($user));

    return response('success', 200);
});
